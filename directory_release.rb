# frozen_string_literal: true

require_relative 'release_file'

# Create ReleaseInfo for a Directory Release
class DirectoryRelease
  TYPE = 'tree'

  def self.process(directories)
    directories.map do |directory|
      puts "Release directory: #{directory.path}"

      all_items = Gitlab.tree(WWW_GITLAB_COM_PATH, request_params(directory))
                        .auto_paginate

      post_files = all_items.select { |item| item.type == 'blob' }

      file_results = post_files.map do |file|
        ReleaseFile.new(file.path, file.name, TYPE).to_release_info
      end

      summary_for_directory(directory, file_results)
    end
  end

  def self.summary_for_directory(directory, file_results)
    ReleaseInfo.new(
      directory.name,
      file_results.map(&:item_count).sum,
      file_results.map(&:bug_count).sum,
      file_results.map(&:performance_count).sum
    )
  end

  def self.request_params(directory)
    {
      path: directory.path,
      per_page: PER_PAGE,
      ref: GIT_REF
    }
  end
end
