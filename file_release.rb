# frozen_string_literal: true

require_relative 'release_file'

# Create ReleaseInfo for a File Release
class FileRelease
  TYPE = 'blob'
  FILE_RELEASE_REGEX =
    /\d{4}_\d{2}_\d{2}_gitlab_(?<release>\d+_\d+)_released/.freeze

  def self.process(files)
    files.map do |file|
      puts "Release file: #{file.path}"

      post = ReleaseFile.new(file.path, file.name, TYPE).to_release_info
      # Adjust name from release file title
      post.name = post.name.match(FILE_RELEASE_REGEX)[:release]
      post
    end
  end
end
