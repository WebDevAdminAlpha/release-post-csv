# frozen_string_literal: true

require_relative 'release_info'
require 'yaml'

# Examine an individual Release File
class ReleaseFile
  BUG_FEATURES_TITLE = 'Bug fixes'
  PERFORMANCE_FEATURES_TITLE = 'Performance improvements'
  ITEM_LINE_REGEX = %r{^([-,*] )?(.+)?\[.+\]\(https:\/\/gitlab.com\/gitlab-org\/.+\/(-\/)?issues\/\d+\)(.+)?}.freeze
  URL_FIELD_REGEX = %r{^https:\/\/gitlab.com\/gitlab-org\/.+\/(-\/)?issues\/\d+$}
  URL_FIELDS_TO_COUNT = [
    'issue_url'
  ]

  attr_reader :path, :name, :type

  def initialize(path, name, type)
    puts "Extracting items from file: #{path}"

    @path = path
    @name = name
    @type = type

    @release_file_content ||=
      Gitlab.file_contents(WWW_GITLAB_COM_PATH, @path, GIT_REF)

    @release_yml ||= YAML.safe_load(@release_file_content)

    return unless valid_item?
  end

  %w[top_features primary_features secondary_features].each do |method|
    define_method method.to_s do
      if features_map[method.to_sym]
        features_map[method.to_sym] - (bug_features | performance_features)
      else
        []
      end
    end

    define_method "#{method}_count" do
      return 0 unless send(method.to_sym)

      count_links_for_features(send(method.to_sym))
    end
  end

  %w[bug_features performance_features].each do |method|
    define_method method.to_s do
      features.select do |feature|
        feature['name'] == self.class.const_get("#{method.upcase}_TITLE")
      end
    end

    define_method "#{method}_count" do
      count_links_for_features(send(method.to_sym))
    end
  end

  def main_feature_count
    top_features_count + primary_features_count + secondary_features_count
  end

  def to_release_info
    if !valid_item?
      default_counts
    else
      ReleaseInfo.new(name, main_feature_count, bug_features_count,
                      performance_features_count)
    end
  end

  private

  def default_counts
    ReleaseInfo.new(name, 0, 0, 0)
  end

  def features_map
    @features_map ||=
      {
        top_features: @release_yml['features']['top'],
        primary_features: @release_yml['features']['primary'],
        secondary_features: @release_yml['features']['secondary']
      }
  end

  def features
    features_map.values.compact.flatten
  end

  def count_links_for_features(items)
    without_link = items.reject do |item|
      valid_url_field?(item)
    end

    with_link = items - without_link

    count_links_with_url_field(with_link) +
    count_links_without_url_field(without_link)
  end

  def count_links_with_url_field(items)
    items.map do |item|
      # Add one for the detected top level link
      valid_description_item_counts([item]).sum + 1
    end.sum
  end

  def count_links_without_url_field(items)
    items.map do |item|
      description_counts = valid_description_item_counts([item]).sum
      # Add 1 for top item if description counts are 0
      description_counts.zero? ? 1 : description_counts
    end.sum
  end

  def valid_url_field?(item)
    url_fields = (item.keys & URL_FIELDS_TO_COUNT)

    return false if url_fields.empty?

    valid_url_fields = url_fields.select do |field|
      item[field] =~ URL_FIELD_REGEX
    end

    !valid_url_fields.empty?
  end

  def valid_description_item_counts(sub_items)
    sub_items.map do |sub_item|
      puts "Counting sub items for #{sub_item['name']}"
      lines = sub_item['description'].split(/(\n)/)
      lines.select do |item_line|
        item_line =~ ITEM_LINE_REGEX
      end.length
    end
  end

  def valid_item?
    @type == 'tree' && !@release_yml['features'] ? false : true
  end
end
