# frozen_string_literal: true

require 'spec_helper'
require 'gitlab'
require_relative '../release_file'

WWW_GITLAB_COM_PATH = 'release-path'
GIT_REF = 'master'

Gitlab.configure do |config|
  config.endpoint = 'fake'
  config.private_token = 'token123'
end

describe ReleaseFile do
  where(:description, :release_file, :expected_counts, :type) do
    [
      ["Release Folder/Release Item File: Issue URL, no links", "release_directory/issue_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Epic URL, no links", "release_directory/epic_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Issue URL, with links", "release_directory/issue_url_with_links.yml", { item_count: 4, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Epic URL, with links", "release_directory/epic_url_with_links.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: No URL, no links", "release_directory/no_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: No URL, with links", "release_directory/no_url_with_links.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Bugs", "release_directory/bugs.yml", { item_count: 0, bug_count: 2, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Performance", "release_directory/performance.yml", { item_count: 0, bug_count: 0, performance_count: 2 }, 'tree'],
      ["Release Folder/Release Item File: Issue URL, mixed links", "release_directory/issue_url_mixed_links.yml", { item_count: 2, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: No URL, mixed links", "release_directory/no_url_mixed_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Secondary Issue URL, no links", "release_directory/secondary_issue_url.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Top Epic URL, with links", "release_directory/top_epic_url.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Inline description links", "release_directory/inline_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Runner post example", "release_directory/runner_post.yml", { item_count: 8, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Geo UX example", "release_directory/geo_ux.yml", { item_count: 5, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Prometheus Metrics example", "release_directory/prometheus_metrics.yml", { item_count: 2, bug_count: 0, performance_count: 0 }, 'tree'],
      ["Release Folder/Release Item File: Non-matching URL example", "release_directory/invalid_url_field.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'tree'],


      ["Release File: Issue URL, no links", "release_files/issue_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Epic URL, no links", "release_files/epic_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Issue URL, with links", "release_files/issue_url_with_links.yml", { item_count: 4, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Epic URL, with links", "release_files/epic_url_with_links.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: No URL, no links", "release_files/no_url_no_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: No URL, with links", "release_files/no_url_with_links.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Bugs", "release_files/bugs.yml", { item_count: 0, bug_count: 2, performance_count: 0 }, 'blob'],
      ["Release File: Performance", "release_files/performance.yml", { item_count: 0, bug_count: 0, performance_count: 2 }, 'blob'],
      ["Release File: Issue URL with links, bugs, performance", "release_files/file_release.yml", { item_count: 50, bug_count: 2, performance_count: 1 }, 'blob'],
      ["Release File: Issue URL, mixed links", "release_files/issue_url_mixed_links.yml", { item_count: 2, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: No URL, mixed links", "release_files/no_url_mixed_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Secondary Issue URL, no links", "release_files/secondary_issue_url.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Top Epic URL, with links", "release_files/top_epic_url.yml", { item_count: 3, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Inline description links", "release_files/inline_links.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Runner post example", "release_files/runner_post.yml", { item_count: 8, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Geo UX example", "release_files/geo_ux.yml", { item_count: 5, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Prometheus Metrics example", "release_files/prometheus_metrics.yml", { item_count: 2, bug_count: 0, performance_count: 0 }, 'blob'],
      ["Release File: Non-matching URL example", "release_files/invalid_url_field.yml", { item_count: 1, bug_count: 0, performance_count: 0 }, 'blob']
    ]
  end

  with_them do
    it "has the correct counts" do
      file_contents = File.read(File.join(File.dirname(__FILE__), "/examples/#{release_file}"))

      allow(Gitlab).to receive(:file_contents).and_return(file_contents)

      release_info = ReleaseFile.new('path', 'name', type).to_release_info

      expect(release_info.item_count).to eq(expected_counts[:item_count])
      expect(release_info.bug_count).to eq(expected_counts[:bug_count])
      expect(release_info.performance_count).to eq(expected_counts[:performance_count])
    end
  end
end
